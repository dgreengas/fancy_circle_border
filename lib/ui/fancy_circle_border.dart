import 'dart:math';

import 'package:flutter/material.dart';

class FancyCircleBorder extends StatelessWidget {
  final Widget? child;
  final BorderSide topLeftBorder;
  final BorderSide topRightBorder;
  final BorderSide bottomRightBorder;
  final BorderSide bottomLeftBorder;
  final StrokeCap strokeCap;

  const FancyCircleBorder({
    Key? key,
    this.child,
    required this.topLeftBorder,
    required this.topRightBorder,
    required this.bottomRightBorder,
    required this.bottomLeftBorder,
    this.strokeCap = StrokeCap.round,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1.0,
      child: CustomPaint(
        painter: BorderPainter(
            bottomLeftBorder: this.bottomLeftBorder,
            topLeftBorder: this.topLeftBorder,
            bottomRightBorder: this.bottomRightBorder,
            topRightBorder: this.topRightBorder,
            strokeCap: this.strokeCap),
        child: child,
      ),
    );
  }
}

class BorderPainter extends CustomPainter {
  final BorderSide topLeftBorder;
  final BorderSide topRightBorder;
  final BorderSide bottomRightBorder;
  final BorderSide bottomLeftBorder;
  final StrokeCap strokeCap;

  BorderPainter({
    required this.topLeftBorder,
    required this.topRightBorder,
    required this.bottomRightBorder,
    required this.bottomLeftBorder,
    required this.strokeCap,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final maxWidth = getMaxWidth();
    final radius = (size.width - maxWidth) / 2;
    final center = Offset(size.width / 2, size.height / 2);

    //offset should be related to radius since 2*pi radians per revolution
    //both radius and maxwidth are in pixels
    //as max width increases, the offset should increase
    // how many radius's is the width
    // if strokeCap is butt, then no offset
    double offset;
    if (strokeCap == StrokeCap.butt) {
      offset = 0;
    } else {
      offset = (2 * maxWidth) / radius;
    }

    /*
    print('offset: $offset \n pi/180: ${pi / 180}');
    print(
        'maxWidth: $maxWidth \nradius: $radius\nwidth/r: ${(2 * maxWidth) / radius}');

    print(
        'Drawing topRightBorder start -p/2 + ${offset / 4} length pi/2 - ${offset / 2}');

    final filledPaint = Paint()
      ..color = Colors.amber
      ..style = PaintingStyle.fill;
    //drawing background circle with lines
    canvas.drawArc(Rect.fromCircle(center: center, radius: radius), -pi / 2,
        pi / 2, true, filledPaint);

    canvas.drawArc(Rect.fromCircle(center: center, radius: radius), -pi, pi / 2,
        true, filledPaint..color = Colors.lightBlueAccent);

    canvas.drawArc(Rect.fromCircle(center: center, radius: radius), 0, pi / 2,
        true, filledPaint..color = Colors.yellowAccent);

    canvas.drawArc(Rect.fromCircle(center: center, radius: radius), pi / 2,
        pi / 2, true, filledPaint..color = Colors.greenAccent); */

    canvas.drawArc(
      Rect.fromCircle(center: center, radius: radius),
      -pi / 2 + offset / 4,
      pi / 2 - offset / 2,
      false,
      topRightBorder.toPaint()..strokeCap = strokeCap,
    );

    canvas.drawArc(
      Rect.fromCircle(center: center, radius: radius),
      -pi + offset / 4,
      pi / 2 - offset / 2,
      false,
      topLeftBorder.toPaint()..strokeCap = strokeCap,
    );

    canvas.drawArc(
      Rect.fromCircle(center: center, radius: radius),
      0 + offset / 4,
      pi / 2 - offset / 2,
      false,
      bottomRightBorder.toPaint()..strokeCap = strokeCap,
    );

    canvas.drawArc(
      Rect.fromCircle(center: center, radius: radius),
      pi / 2 + offset / 4,
      pi / 2 - offset / 2,
      false,
      bottomLeftBorder.toPaint()..strokeCap = strokeCap,
    );
  }

  @override
  bool shouldRepaint(covariant BorderPainter oldDelegate) {
    if (oldDelegate.bottomLeftBorder != bottomLeftBorder ||
        oldDelegate.topLeftBorder != topLeftBorder ||
        oldDelegate.bottomRightBorder != bottomRightBorder ||
        oldDelegate.topRightBorder != topRightBorder) {
      return true;
    } else {
      return false;
    }
  }

  double getMaxWidth() {
    //compare each border width
    return max(
        bottomLeftBorder.width,
        max(topLeftBorder.width,
            max(topRightBorder.width, topLeftBorder.width)));
  }
}
